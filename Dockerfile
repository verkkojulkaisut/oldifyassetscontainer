FROM mhart/alpine-node:15

WORKDIR /app
COPY . .

#CMD ["node", "watch.js"]
RUN apk update \
  && apk add --virtual build-dependencies \
  build-base \
  gcc \
  wget \
  git

RUN apk update && apk upgrade && \
    apk add --no-cache bash git pkgconfig openssh automake autoconf libtool nasm zlib-dev make python2 python3 curl

RUN apk add --no-cache libstdc++;
#    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash; \
#    echo 'source $HOME/.profile;' >> $HOME/.zshrc

RUN yarn global add node-gyp
RUN yarn global add gulp


CMD cd oldifymoduleassets && yarn install && gulp
